﻿package {
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import network.Connector;
	import network.ConnectorInterface;
	public class AppManager implements ConnectorInterface {		private static var instance:AppManager = null;		public var socket:Connector = new Connector();		[Bindable]		private var main:Main = Main.getInstance();
		// Timer for reconnect to server
		private var timer:Timer;		public static function getInstance():AppManager {			if (instance == null) {				instance = new AppManager();			}			return instance;		}
		
		//Interface ConnectorInterface
		public function onStart():void {
			trace("Connector start");
			socket.isConnect = true;
			timer.stop();
		}
		
		public function onTerminate():void {
			trace("Connector terminate");
			socket.isConnect = false;
			startTimerReconnectServer();
		}
		
		public function onMessage(data:String):void {
			main.debug(data);
		}
		
		public function onError():void {
			trace("onError from main");
			socket.isConnect = false;
			startTimerReconnectServer();
		}
		
		public function startApp():void {
			//socket.host = "sunchoku.no-ip.org";
			socket.host = Config.server;
			socket.port = Config.port;
			socket.delegate = this;	
			main = Main.getInstance();
			setupTimerForReconnectToServer();
		}
				public function sendPacket(msg:String):void{			socket.sendMessage(msg);		}
			public function sendMessage(message):void {
			sendPacket(message);		}
				public function receiveMessage(message:String):void {			main.debug(message);		}
		// Timer for reconnect to server
		private function startTimerReconnectServer():void {
			timer.reset();
			timer.start();
		}
		
		private function setupTimerForReconnectToServer():void {
			timer = new Timer(1000, 60);
			timer.addEventListener(TimerEvent.TIMER, timerHandler);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, timerComplete); 
			timer.start();
		}
		
		private function timerHandler(event:TimerEvent):void {
			trace("connect server");
			if(!socket.connected) {
				socket.startConnect();
			}
		}
		
		private function timerComplete(event:TimerEvent):void {
			timer.reset();
			timer.start();
			trace("timer complete");
		}	}}